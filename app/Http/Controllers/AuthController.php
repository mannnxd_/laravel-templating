<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function bio(){
        return view('halaman.register');
    }
    public function kirim(Request $request){
        $depan = $request['first'];
        $belakang = $request['last'];
        return view('halaman.welcome', compact('depan','belakang',));
    }
}
